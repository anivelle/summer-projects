// Contours.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	// Load image as grayscale so I don't have to convert it later
	Mat image = imread("../Images/shapes.png", 0);

	// Show original image
	namedWindow("Raw");
	imshow("Raw", image);
	
	// Not sure what use this has, since it's already a black and white image
	threshold(image, image, 128, 255, THRESH_BINARY);

	// Create arrays to hold contours and optional hierarchy 
	std::vector<std::vector<Point>> contours;
	std::vector<std::vector<Point>> contours0;
	std::vector<Vec4i> hierarchy;

	// Find the contours of the image and store them
	findContours(image, contours0, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	// Resize the contour array to be the same size as the temporary one
	contours.resize(contours0.size());

	for (size_t i = 0; i < contours0.size(); i++)
	{
		// Approximate all the polygons within the image based on contours.
		// I imagine this sets them up in a sort of hierarchy, based on 
		// what I saw in the debugger
		approxPolyDP(Mat(contours0[i]), contours[i], 3, true);
	}

	namedWindow("Contours");
	Mat contourImg = image.clone();
	// Converts the contour image to BGR because otherwise you can't see the different colors
	cvtColor(contourImg, contourImg, COLOR_GRAY2BGR);

	for (size_t i = 0; i < contours.size(); i++)
	{
		int sides = contours[i].size();
		Scalar color;
		// Determines contour color based on number of sides
		if (sides == 3)
		{
			color = Scalar(255, 0, 0);
		}
		else if (sides == 4)
		{
			color = Scalar(0, 255, 0);
		}
		else if (sides == 5)
		{
			color = Scalar(0, 0, 255);
		}
		else
		{
			color = Scalar(255, 0, 255);
		}
		// Draws contours for each shape individually
		drawContours(contourImg, contours, i, color, 2, LINE_AA, hierarchy);
	}

	imshow("Contours", contourImg);
	waitKey(0);

	return 0;
}