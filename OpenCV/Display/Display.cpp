// OpenCV.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "stdafx.h"

#include <opencv2/opencv.hpp>

#include <iostream>
#include <string>

using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Could not open or find image" << std::endl;
		system("pause");
		return -1;
	}

	String windowName = "My HelloWorld Window";
	
	namedWindow(windowName);

	imshow(windowName, image);

	waitKey(0);

	destroyWindow(windowName);

	return 0;
}

