#include "stdafx.h"

#include<opencv2/opencv.hpp>
#include<iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	// Open video file for viewing
	//VideoCapture cap("../Images/vid.mp4");

	// Initialize video camera
	VideoCapture cap(0);

	// End if video file was not opened
	if (cap.isOpened() == false)
	{
		std::cout << "Cannot open the video file/camera" << std::endl;
		std::cin.get();
		return -1;
	}

	double dWidth = cap.get(CAP_PROP_FRAME_WIDTH);
	double dHeight = cap.get(CAP_PROP_FRAME_HEIGHT);

	std::cout << "Resolution: " << dWidth << "x" << dHeight << std::endl;
	
	// Get video framerate
	//double fps = cap.get(CAP_PROP_FPS);
	//std::cout << "FPS: " << fps << std::endl;

	String windowName = "My First Video";

	namedWindow(windowName);

	while (true)
	{
		Mat frame;
		bool bSuccess = cap.read(frame); // Read a new frame

		// Break the while loop at the end of the video
		if (!bSuccess)
		{
			std::cout << "Found the end of the video" << std::endl;
			break;
			//cap.set(CAP_PROP_POS_MSEC, 0); // Loops the video for fun
			//cap.read(frame);
		}

		// Show frame in created window
		imshow(windowName, frame);

		// Wait for key input for 10 ms. If Escape key pressed, end the video. 
		// Otherwise, continue.
		if (waitKey(10) == 27)
		{
			std::cout << "Escape key pressed by user. Stopping the video" << std::endl;
			break;
		}
	}

	return 0;
}