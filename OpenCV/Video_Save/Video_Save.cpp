// Video_Save.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main()
{
	VideoCapture cap(0); // Capture video from webcam

	if (!cap.isOpened()) // Make sure camera is working
	{
		std::cout << "Could not open the webcam" << std::endl;
		std::cin.get();
		return -1;
	}
	
	int frame_width = static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH));
	int frame_height = static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT));

	Size frame_size(frame_width, frame_height); // Set frame size of video file
	int frames_per_second = static_cast<int>(cap.get(CAP_PROP_FPS)); // Set fps of file

	// Instantiate VideoWriter object to write to file
	VideoWriter cVideoWriter("../Images/myVideo.mp4", VideoWriter::fourcc('M', 'P', '4', '2'), frames_per_second, frame_size);

	if (!cVideoWriter.isOpened()) // Make sure writing to file is possible
	{
		std::cout << "Couldn't write video to a file" << std::endl;
		std::cin.get();
		return -1;
	}

	String videoName = "My Camera Feed"; 
	namedWindow(videoName); // Instantiate a new window

	while (true)
	{
		Mat frame;
		bool isSuccess = cap.read(frame); // Read the next frame of the camera feed

		if (!isSuccess)
		{
			std::cout << "Video camera is disconnected" << std::endl;
			std::cin.get();
			break;
		}

		// Make changes to video <- This is important for ROV

		cVideoWriter.write(frame); // Write frame to video file

		imshow(videoName, frame); // Show frame in window

		if (waitKey(10) == 27) // Wait 10 ms for a key
		{
			std::cout << "Esc key is pressed by the user. Stopping the video" << std::endl;
			break;
		}
	}

	cVideoWriter.release(); /// End writing to file

	return 0;
}

