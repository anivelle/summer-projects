// Histogram_Equalization.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int Equalize_Grayscale();
int Equalize_Color();
//Equalizes a grayscale image
int main()
{
	// return Equalize_Grayscale();
	return Equalize_Color();
}

int Equalize_Grayscale()
{
	Mat image = imread("../Images/space.jpg", IMREAD_GRAYSCALE);

	if (image.empty())
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat hist_equalized_image;
	equalizeHist(image, hist_equalized_image); // Equalize image 

	String windowOrig = "Original Image";
	String windowEqual = "Equalized Image";

	namedWindow(windowOrig); 
	namedWindow(windowEqual);

	imshow(windowOrig, image);
	imshow(windowEqual, hist_equalized_image);
	
	waitKey(0);

	destroyAllWindows();
	return 0;
}

int Equalize_Color()
{
	Mat image = imread("../Images/space.jpg"); // Open image 

	if (image.empty())
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat hist_equalized_image;
	cvtColor(image, hist_equalized_image, COLOR_BGR2YCrCb); // Convert image from BGR to YCC color space

	std::vector<Mat> vec_channels;
	split(hist_equalized_image, vec_channels); // Split image into its three channels

	equalizeHist(vec_channels[0], vec_channels[0]); // Equalize the Y channel

	merge(vec_channels, hist_equalized_image); // Merge channels back together

	cvtColor(hist_equalized_image, hist_equalized_image, COLOR_YCrCb2BGR); // Convert color space back to BGR

	String windowOrig = "Original Image";
	String windowEqual = "Equalized Image";

	namedWindow(windowOrig);
	namedWindow(windowEqual);

	imshow(windowOrig, image);
	imshow(windowEqual, hist_equalized_image);

	waitKey(0);

	destroyAllWindows();

	return 0;
}