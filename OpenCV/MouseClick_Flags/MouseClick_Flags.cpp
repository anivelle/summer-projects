// MouseClick_Flags.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

void OnClick(int event, int x, int y, int flags, void* userData);

int main()
{
	Mat image = imread("../Images/space.jpg");

	String winName = "My Image";
	namedWindow(winName);

	setMouseCallback(winName, OnClick);

	imshow(winName, image);

	waitKey(0);

	return 0;
}

void OnClick(int event, int x, int y, int flags, void* userData)
{
	switch (flags)
	{
	case EVENT_FLAG_CTRLKEY + EVENT_FLAG_LBUTTON:
		std::cout << "Left mouse button clicked while holding Ctrl - position: " << x << ", " << y << std::endl;
		break;
	case EVENT_FLAG_RBUTTON + EVENT_FLAG_SHIFTKEY:
		std::cout << "Right mouse button clicked while holding Shift - position: " << x << ", " << y << std::endl;
		break;
	case EVENT_FLAG_ALTKEY:
		if (event == EVENT_MOUSEMOVE)
		{
			std::cout << "Mouse moved while holding Alt - position: " << x << ", " << y << std::endl;
			break;
		}
	default:
		break;
	}
}
