// Filter_Homogeneous.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty()) // Make sure image exists
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat img_blurred_3x3;
	GaussianBlur(image, img_blurred_3x3, Size(3, 3), 2); // Blur with 3x3 kernel

	Mat img_blurred_5x5;
	GaussianBlur(image, img_blurred_5x5, Size(5, 5), 2); // Blur with 5x5 kernel

	String kernel3x3 = "Blurred with 3x3 kernel";
	namedWindow(kernel3x3); // Instantiate window for 3x3 kernel

	String kernel5x5 = "Blurred with 5x5 kernel";
	namedWindow(kernel5x5); // Instantiate window for 5x5 kernel

	imshow(kernel3x3, img_blurred_3x3); // Show 3x3 blur
	imshow(kernel5x5, img_blurred_5x5); // Show 5x5 blur

	waitKey(0);

	destroyAllWindows();

	return 0;
}

