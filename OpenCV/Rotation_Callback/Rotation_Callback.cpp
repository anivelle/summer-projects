// Rotation_Callback.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

void TrackbarCall(int, void*);

Mat image;
String rotated;

// Global variables for use in both main and the callback function
int angle = 0;
int scale = 1;
int borderMode = 0;
int centerX;
int centerY;

int main()
{
	image = imread("../Images/space.jpg");

	centerX = image.cols / 2; // Get X coord of image center
	centerY = image.rows / 2; // Get Y coord of image center

	String winOrig = "Original Image";
	namedWindow(winOrig);
	imshow(winOrig, image);

	rotated = "Rotated Image";
	namedWindow(rotated);
	// Trackbars to control aspects of the image rotation
	createTrackbar("Angle", rotated, &angle, 360, TrackbarCall);
	createTrackbar("Scale", rotated, &scale, 100, TrackbarCall);
	createTrackbar("Border Mode", rotated, &borderMode, 5, TrackbarCall);

	int dummy = 0;

	TrackbarCall(dummy, &dummy); // Not sure what this is for, it was not explained and I don't know enough about C++

	waitKey(0);

	return 0;
}

void TrackbarCall(int, void*)
{
	Mat rotation = getRotationMatrix2D(Point(centerX, centerY), angle, scale); // Create rotation matrix

	Mat rotatedImg; // Instantiate material for rotated image and apply transformation
	warpAffine(image, rotatedImg, rotation, image.size(), INTER_LINEAR, borderMode, Scalar());

	imshow(rotated, rotatedImg);
}
