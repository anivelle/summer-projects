// CoralDetection.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main()
{
	Mat coralOld = imread("../Images/Annotation 2020-07-16 000930.png");
	Mat coralNew = imread("../Images/Annotation 2020-07-16 001011.png");

	Mat oImgHSV; // Old coral HSV
	Mat nImgHSV; // New coral HSV
	Mat oImgThresholded; // Old image thresholded
	Mat nImgThresholded; // New image thresholded

	if (coralOld.empty() || coralNew.empty())
	{
		std::cout << "Could not open one or more of the images" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat subtracted;
	String subtraction = "Subtracted images";
	namedWindow(subtraction);

	// Old coral image window
	String oldC = "Old Coral";
	namedWindow(oldC, WINDOW_NORMAL);

	// Threshold values for old coral image
	int lowH = 0;
	int highH = 180;

	int lowS = 0;
	int highS = 255;

	int lowV = 0;
	int highV = 255;

	// Trackbars for old coral image
	createTrackbar("LowH", oldC, &lowH, 180); // Hue (0 - 180)
	createTrackbar("HighH", oldC, &highH, 255);

	createTrackbar("LowS", oldC, &lowS, 255); // Saturation (0 - 255)
	createTrackbar("HighS", oldC, &highS, 255);

	createTrackbar("LowV", oldC, &lowV, 255); // Value (0 - 255)
	createTrackbar("HighV", oldC, &highV, 255);

	// New coral image window
	String newC = "New Coral";
	namedWindow(newC, WINDOW_NORMAL);

	// Threshold values for new coral image
	int nLowH = 0; 
	int nHighH = 180;

	int nLowS = 0;
	int nHighS = 255;

	int nLowV = 0;
	int nHighV = 255;
	
	// Trackbars for new coral image
	createTrackbar("LowH", newC, &nLowH, 180); // Hue (0 - 180)
	createTrackbar("HighH", newC, &nHighH, 255);

	createTrackbar("LowS", newC, &nLowS, 255); // Saturation (0 - 255)
	createTrackbar("HighS", newC, &nHighS, 255);

	createTrackbar("LowV", newC, &nLowV, 255); // Value (0 - 255)
	createTrackbar("HighV", newC, &nHighV, 255);

	// Pads out old coral image because it's smaller thant the new coral image. Won't be an issue with the ROV
	// since it's the same camera. <----- Wait that could mean that the coral could be smaller in one image
	// Unless we have the original and we only have to capture the new one. I need to read the manual again.
	copyMakeBorder(coralOld, coralOld, coralNew.rows - coralOld.rows, 0, (coralNew.cols - coralOld.cols) / 2, (coralNew.cols - coralOld.cols) / 2, BORDER_CONSTANT, Scalar());

	while (true)
	{
		cvtColor(coralOld, oImgHSV, COLOR_BGR2HSV); // Convert old coral image from BGR to HSV
		cvtColor(coralNew, nImgHSV, COLOR_BGR2HSV); // Convert new coral image from BGR to HSV

		inRange(oImgHSV, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), oImgThresholded);
		inRange(nImgHSV, Scalar(nLowH, nLowS, nLowV), Scalar(nHighH, nHighS, nHighV), nImgThresholded);

		// Morphological opening (erode then dilate, remove small objects from foreground)
		erode(oImgThresholded, oImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		dilate(oImgThresholded, oImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		erode(nImgThresholded, nImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		dilate(nImgThresholded, nImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		// Morphological closing (dilate then erode, remove small holes in foreground)
		dilate(oImgThresholded, oImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		erode(oImgThresholded, oImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		dilate(nImgThresholded, nImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		erode(nImgThresholded, nImgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		subtract(nImgThresholded, oImgThresholded, subtracted);

		imshow(subtraction, subtracted);
		imshow(oldC, oImgThresholded);
		imshow(newC, nImgThresholded);

		if (waitKey(30) == 27)
		{
			break;
		}
	}

	return 0;
}