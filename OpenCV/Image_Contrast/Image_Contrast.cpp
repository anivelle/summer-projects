// Image_Contrast.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Could not open the image" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat imageContrastHigh2;
	image.convertTo(imageContrastHigh2, -1, 2, 0); // Increase the contrast by 2

	Mat imageContrastHigh4;
	image.convertTo(imageContrastHigh4, -1, 4, 0); // Increase the contrast by 4

	Mat imageContrastLow0_5;
	image.convertTo(imageContrastLow0_5, -1, 0.5, 0); // Decrease the contrast by 0.5

	Mat imageContrastLow0_25;
	image.convertTo(imageContrastLow0_25, -1, 0.25, 0); // Decrease the contrast by 0.25


	//Defining window names for above images
	String windowOrig = "Original Image";
	String window2x = "Contrast Increased by 2";
	String window4x = "Contrast Increased by 4";
	String window0_5x = "Contrast Decreased by 0.5";
	String window0_25x = "Contrast Decreased by 0.25";

	//Create and open windows for above images
	namedWindow(windowOrig);
	namedWindow(window2x);
	namedWindow(window4x);
	namedWindow(window0_5x);
	namedWindow(window0_25x);

	//Show above images inside the created windows.
	imshow(windowOrig, image);
	imshow(window2x, imageContrastHigh2);
	imshow(window4x, imageContrastHigh4);
	imshow(window0_5x, imageContrastLow0_5);
	imshow(window0_25x, imageContrastLow0_25);

	waitKey(0); // Wait for any key stroke

	destroyAllWindows(); //destroy all open windows

	return 0;
}
