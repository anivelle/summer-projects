// ObjectTracking.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main()
{
	VideoCapture cap(0);

	if (!cap.isOpened())
	{
		std::cout << "Could not open webcam" << std::endl;
		std::cin.get();
		return -1;
	}

	String cam = "Rainbow";
	namedWindow(cam, WINDOW_NORMAL);

	int lowH = 0;
	int highH = 180;

	int lowS = 0;
	int highS = 255;

	int lowV = 0;
	int highV = 255;

	createTrackbar("LowH", cam, &lowH, 180); // Hue (0 - 180)
	createTrackbar("HighH", cam, &highH, 180);

	createTrackbar("LowS", cam, &lowS, 255); // Saturation (0 - 255)
	createTrackbar("HighS", cam, &highS, 255);

	createTrackbar("LowV", cam, &lowV, 255); // Value (0 - 255)
	createTrackbar("HighV", cam, &highV, 255);

	int iLastX = -1;
	int iLastY = -1;
	// Create a temporary frame from the video
	Mat imgTmp;
	cap.read(imgTmp);

	// Create black image with same size as output
	Mat lines = Mat::zeros(imgTmp.size(), CV_8UC3);

	while (true)
	{
		Mat imgOrig;
		bool bSuccess = cap.read(imgOrig);
		if (!bSuccess) // Make sure video is working
		{
			std::cout << "Cannot read frame from video" << std::endl;
			break;
		}

		Mat imgHSV;

		cvtColor(imgOrig, imgHSV, COLOR_BGR2HSV); // Convert frame from BGR to HSV

		Mat imgThresholded;
		inRange(imgHSV, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), imgThresholded);

		// Morphological opening (erode then dilate, remove small objects from foreground)
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		// Morphological closing (dilate then erode, remove small holes in foreground)
		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));


		// Capture moments of thresholded image (Up to 3rd order, whatever that means)
		Moments oMoments = moments(imgThresholded);

		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;

		// Check if area is greater than 10000 to make sure it's not just noise 
		// (could possibly reduce area for smaller objects, not sure)
		if (dArea > 10000)
		{
			int posX = dM10 / dArea;
			int posY = dM01 / dArea;

			if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
			{
				line(lines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0, 0, 255), 2);
				std::cout << posX << ", " << posY << std::endl;
			}
			
			iLastX = posX;
			iLastY = posY;
		}
		imgOrig = imgOrig + lines;
		imshow("Lines", lines);
		imshow("Thresholded image", imgThresholded);
		imshow("Original", imgOrig);

		if (waitKey(30) == 27)
		{
			break;
		}
	}

	return 0;
}