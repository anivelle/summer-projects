// ObjectDetection.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	//VideoCapture cap(0);
	Mat rainbow = imread("../Images/our coral.jpg");

	if (rainbow.empty())//!cap.isOpened())
	{
		std::cout << "Could not open webcam" << std::endl;
		std::cin.get();
		return -1;
	}

	String cam = "Rainbow";
	namedWindow(cam, WINDOW_NORMAL);

	int lowH = 0;
	int highH = 180;

	int lowS = 0;
	int highS = 255;

	int lowV = 0;
	int highV = 255;

	createTrackbar("LowH", cam, &lowH, 180); // Hue (0 - 180)
	createTrackbar("HighH", cam, &highH, 180);

	createTrackbar("LowS", cam, &lowS, 255); // Saturation (0 - 255)
	createTrackbar("HighS", cam, &highS, 255);

	createTrackbar("LowV", cam, &lowV, 255); // Value (0 - 255)
	createTrackbar("HighV", cam, &highV, 255);

	while (true)
	{
		//Mat imgOrig;
		//bool bSuccess = cap.read(imgOrig);
		//if (!bSuccess) // Make sure video is working
		//{
		//	std::cout << "Cannot read frame from video" << std::endl;
		//	break;
		//}

		Mat imgHSV;

		cvtColor(rainbow, imgHSV, COLOR_BGR2HSV); // Convert frame from BGR to HSV

		Mat imgThresholded;
		inRange(imgHSV, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), imgThresholded);

		// Morphological opening (erode then dilate, remove small objects from foreground)
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		// Morphological closing (dilate then erode, remove small holes in foreground)
		dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
		erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

		imshow("Thresholded image", imgThresholded);
		imshow("Original", rainbow);

		if (waitKey(30) == 27)
		{
			break;
		}
	}

	return 0;
}