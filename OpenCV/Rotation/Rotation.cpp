// Rotation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main()
{
	Mat image = imread("../Images/space.jpg");

	String winOrig = "Original Image";
	namedWindow(winOrig);
	imshow(winOrig, image);

	String rotated = "Rotated Image";
	namedWindow(rotated);

	int angle = 0;
	createTrackbar("Angle", rotated, &angle, 360);

	int imageHeight = image.rows / 2;
	int imageWidth = image.cols / 2;

	while (true)
	{
		Mat rotation = getRotationMatrix2D(Point(imageWidth, imageHeight), angle, 1);

		Mat rotatedImg;
		warpAffine(image, rotatedImg, rotation, image.size());

		imshow(rotated, rotatedImg);

		if (waitKey(30) == 27)
		{
			break;
		}
	}

	return 0;
}
