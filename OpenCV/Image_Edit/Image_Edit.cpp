// Image_Edit.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}

	Mat imageBrightnessHigh50;
	image.convertTo(imageBrightnessHigh50, -1, 1, 50); // Increase brightness by 50

	Mat imageBrightnessHigh100;
	image.convertTo(imageBrightnessHigh100, -1, 1, 100); // Increase brightness by 100

	Mat imageBrightnessLow50; 
	image.convertTo(imageBrightnessLow50, -1, 1, -50); // Decrease brightness by 50

	Mat imageBrightnessLow100;
	image.convertTo(imageBrightnessLow100, -1, 1, -100); // Decrease brightness by 100

	// Defining window names for above images
	String windowOrig = "Original Image";
	String windowPlus50 = "Brightness Increased by 50";
	String windowPlus100 = "Brightness Increased by 100";
	String windowMinus50 = "Brightness Decreased by 50";
	String windowMinus100 = "Brightness Decreased by 100";

	// Instantiating windows for above images
	namedWindow(windowOrig);
	namedWindow(windowPlus50);
	namedWindow(windowPlus100);
	namedWindow(windowMinus50);
	namedWindow(windowMinus100);

	// Show edited images
	imshow(windowOrig, image);
	imshow(windowPlus50, imageBrightnessHigh50);
	imshow(windowPlus100, imageBrightnessHigh100);
	imshow(windowMinus50, imageBrightnessLow50);
	imshow(windowMinus100, imageBrightnessLow100);

	waitKey(0);

	destroyAllWindows();

	return 0;
}
