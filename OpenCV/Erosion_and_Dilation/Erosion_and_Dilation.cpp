// Erosion_and_Dilation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

void Dilation(Mat InputImage);

void Erosion(Mat InputImage);

int main()
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Could not open the image" << std::endl;
		std::cin.get();
		return -1;
	}
	Dilation(image);
	//Erosion(image);

	return 0;
}

void Dilation(Mat InputImage)
{
	Mat image = InputImage;

	Mat img_dilated_3x3;
	dilate(image, img_dilated_3x3, getStructuringElement(MORPH_RECT, Size(3, 3))); // Erode image with 3x3 kernel

	Mat img_dilated_5x5;
	dilate(image, img_dilated_5x5, getStructuringElement(MORPH_RECT, Size(5, 5))); // Erode image with 5x5 kernel

	String dilated3x3 = "Eroded with 3x3 Kernel";
	namedWindow(dilated3x3);

	String dilated5x5 = "Eroded with 5x5 Kernel";
	namedWindow(dilated5x5);

	imshow(dilated3x3, img_dilated_3x3);
	imshow(dilated5x5, img_dilated_5x5);

	waitKey(0);
}

void Erosion(Mat InputImage)
{
	Mat image = InputImage;

	Mat img_eroded_3x3;
	erode(image, img_eroded_3x3, getStructuringElement(MORPH_RECT, Size(3, 3))); // Erode image with 3x3 kernel

	Mat img_eroded_5x5;
	erode(image, img_eroded_5x5, getStructuringElement(MORPH_RECT, Size(5, 5))); // Erode image with 5x5 kernel

	String eroded3x3 = "Eroded with 3x3 Kernel"; 
	namedWindow(eroded3x3);

	String eroded5x5 = "Eroded with 5x5 Kernel";
	namedWindow(eroded5x5);

	imshow(eroded3x3, img_eroded_3x3);
	imshow(eroded5x5, img_eroded_5x5);

	waitKey(0);
}

