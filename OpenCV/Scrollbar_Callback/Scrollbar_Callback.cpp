// Scrollbar_Callback.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

void Brightness(int bValue, void* userData);

void Contrast(int cValue, void* userData);

Mat image;
String windowName;

int main(int argc, char** argv[])
{
	image = imread("../Images/space.jpg");
	windowName = "My Window";

	if (image.empty())
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}

	namedWindow(windowName);
	
	int bValue = 50;
	int cValue = 50;

	createTrackbar("Brightness", windowName, &bValue, 100, Brightness, &cValue); // Instantiate trackbar to modify brightness

	createTrackbar("Contrast", windowName, &cValue, 100, Contrast, &bValue); // Instantiate trackbar to modify contrast

	imshow(windowName, image);

	waitKey(0);

	return 0;
}

void Brightness(int bValue, void* userData)
{
	Mat dst;
	double contrastVal = *(static_cast<int*>(userData)) / 50.0; // Cast userData (contrast value) to an int then divide by 50 for use in trackbar
	int brightnessVal = bValue - 50; 

	image.convertTo(dst, -1, contrastVal, brightnessVal);

	imshow(windowName, dst); // Display edited image
}

void Contrast(int cValue, void* userData)
{
	Mat dst;
	double contrastVal = cValue / 50.0;
	int brightnessVal = *(static_cast<int*>(userData)) - 50; // Cast userData (brightness value) to an int and subtract 50 for use in trackbar

	image.convertTo(dst, -1, contrastVal, brightnessVal);

	imshow(windowName, dst); // Display edited image
}