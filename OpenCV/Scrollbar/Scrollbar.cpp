// Scrollbar.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Could not open image" << std::endl;
		std::cin.get();
		return -1;
	}
	
	String windowName = "My Window";
	namedWindow(windowName); // Instantiate a window

	int sliderValue1 = 50;
	createTrackbar("Brightness", windowName, &sliderValue1, 100); // Instantiate a scrollbar to control brightness

	int sliderValue2 = 50;
	createTrackbar("Contrast", windowName, &sliderValue2, 100); // Instantiate a scrollbar to control contrast

	while (true)
	{
		Mat dst;
		int brightness = sliderValue1 - 50;
		double contrast = sliderValue2 / 50.0;
		image.convertTo(dst, -1, contrast, brightness); // Apply brightness and contrast to image

		imshow(windowName, dst);

		int pressed = waitKey(10);

		if (pressed == 27)
		{
			break;
		}
		else if (pressed == 112)
		{
			imwrite("../Images/wallpaper.jpg", dst);
		}

	}

	return 0;
}