// MouseClick.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

void OnClick(int event, int x, int y, int flags, void* userData);

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg");

	if (image.empty())
	{
		std::cout << "Couldn't open the image" << std::endl;
		std::cin.get();
		return -1;
	}

	String winName = "My Window";
	namedWindow(winName);

	setMouseCallback(winName, OnClick);

	imshow(winName, image);

	waitKey(0);

	return 0;
}

void OnClick(int event, int x, int y, int flags, void* userData)
{
	switch (event)
	{
	case EVENT_LBUTTONDOWN:
		std::cout << "Left mouse button clicked - position: " << x << ", " << y << std::endl;
		break;
	case EVENT_RBUTTONDOWN:
		std::cout << "Right mouse button clicked - position: " << x << ", " << y << std::endl;
		break;
	case EVENT_MBUTTONDOWN:
		std::cout << "Middle mouse button clicked - position: " << x << ", " << y << std::endl;
		break;
	case EVENT_MOUSEMOVE:
		std::cout << "Mouse moved - position: " << x << ", " << y << std::endl;
	default:
		break;
	}
}
