// Image_Save.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "stdafx.h"

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;

int main(int argc, char** argv[])
{
	Mat image = imread("../Images/space.jpg"); // Read image from file

	if (image.empty())
	{
		std::cout << "Could not open or find the image" << std::endl;
		std::cin.get(); 
		return -1;
	}

	// Make changes to image. Not yet covered. 

	bool isSuccess = imwrite("../Images/myImage.jpg", image); // Write image to file

	if (!isSuccess) // Makes sure image was properly saved
	{
		std::cout << "Could not write image to file" << std::endl;
		std::cin.get();
		return -1;
	}

	std::cout << "Successfully wrote image to file" << std::endl;

	String windowName = "The Saved Image"; 
	namedWindow(windowName);
	imshow(windowName, image); // Displays saved image

	waitKey(0);

	return 0;

}

