#include "MainWindow.h"
#include "ui_MainWindow.h"


void MainWindow::Append()
{
    // Grab message from send box
    QString messageString = this->ui->messageEdit->text();
    // Clear box
    this->ui->messageEdit->clear();
    // Append to model
    this->messages->Append(messageString);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->messages = new Messages();
    this->ui->messagesListView->setModel((QStringListModel*)this->messages);
    // Send to the model when button is pressed
    connect(this->ui->sendButton, &QPushButton::pressed, this, &MainWindow::Append);
    // Send to the model when Enter is pressed
    connect(this->ui->messageEdit, &QLineEdit::editingFinished, this, &MainWindow::Append);
}

MainWindow::~MainWindow()
{
    delete ui;
}
