#ifndef MESSAGES_H
#define MESSAGES_H
#include <QStringListModel>

class Messages : public QStringListModel
{
public:
    Messages();
    void Append(QString message);
};

#endif // MESSAGES_H
